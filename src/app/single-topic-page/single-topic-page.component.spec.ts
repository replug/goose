import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleTopicPageComponent } from './single-topic-page.component';

describe('SingleTopicPageComponent', () => {
  let component: SingleTopicPageComponent;
  let fixture: ComponentFixture<SingleTopicPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleTopicPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleTopicPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
